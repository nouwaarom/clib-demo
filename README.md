# Clib demo
A demo for clib with gitlab

## Usage
- Create a gitlab api token. (Preferences -> Access Tokens) with `read_repository` rights.
- Copy `clib_secrets.json.dist` to `clib_secrets.json`
- Add the gitlab api token to `clib_secrets.json`
- Install the dependency using `clib-install -c -f`
